<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id
 * @property string $nome
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 *
 * @property Projetos[] $projetos
 */
class Categoria extends \common\models\ApplicationModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'status' => 'Status',
            'created_at' => 'Criado em',
            'created_by' => 'Criado por',
            'updated_at' => 'Atualizado em',
            'updated_by' => 'Atualizado por',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjetos()
    {
        return $this->hasMany(Projetos::className(), ['categoria_id' => 'id']);
    }

    /** Retorna um array para alimentar o widget Select2
     * @return Array
     */
    public static function select2()
    {
        $models = self::findAll(['deleted_at' => null, 'status' => 1]);
        return ArrayHelper::map($models, 'id', 'nome');    
    }
}
