<?php

namespace common\models;

use Yii;

/** Este model seve como raiz para os outros models do sistema
 */
class ApplicationModel extends \yii\db\ActiveRecord
{
    /**
     * Reescrevendo a função do beforeSave para que ao detectar que um save o sistema saiba atualiza os atributos de createds e updateds.
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by = Yii::$app->user->identity->id;
            $this->updated_by = Yii::$app->user->identity->id;
        } else $this->updated_by = Yii::$app->user->identity->id;

        return true;
    }

    /** 
     * Reescrevendo função de deletar para que não seja deletado realmente e sim sinalizado.
     */
    public function delete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        $this->deleted_by = Yii::$app->user->identity->id;
        return $this->save();
    }
}
