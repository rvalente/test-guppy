<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "projetos".
 *
 * @property int $id
 * @property int $categoria_id
 * @property int $cliente_id
 * @property string $nome
 * @property string $descricao
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 *
 * @property Categorias $categoria
 * @property Clientes $cliente
 */
class Projeto extends \common\models\ApplicationModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projetos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria_id', 'cliente_id', 'nome'], 'required'],
            [['categoria_id', 'cliente_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['descricao'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nome'], 'string', 'max' => 200],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['categoria_id' => 'id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoria_id' => 'Categoria',
            'cliente_id' => 'Cliente',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
            'created_at' => 'Criado em',
            'created_by' => 'Criado por',
            'updated_at' => 'Atualizado em',
            'updated_by' => 'Atualizado por',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['id' => 'categoria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['id' => 'cliente_id']);
    }
}
