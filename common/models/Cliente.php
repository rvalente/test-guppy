<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string $nome
 * @property string $email
 * @property string $nascimento
 * @property int $genero
 * @property string $telefone
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Cliente extends ApplicationModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'email'], 'required'],
            [['nascimento', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['genero', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['nome'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 150],
            [['telefone'], 'string', 'max' => 50],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'email' => 'Email',
            'nascimento' => 'Nascimento',
            'genero' => 'Genero',
            'telefone' => 'Telefone',
            'created_at' => 'Criado em',
            'created_by' => 'Criado por',
            'updated_at' => 'Atualizado em',
            'updated_by' => 'Atualizado por',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }

    /** Retorna um array para alimentar o widget Select2
     * @return Array
     */
    public static function select2()
    {
        $models = self::findAll(['deleted_at' => null]);
        return ArrayHelper::map($models, 'id', 'nome');    
    }
}
