<?php

use yii\db\Schema;
use yii\db\Migration;

class m181205_055856_clientes extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%clientes}}',
            [
                'id'=> $this->primaryKey(11),
                'nome'=> $this->string(255)->notNull(),
                'email'=> $this->string(150)->notNull(),
                'nascimento'=> $this->date()->null()->defaultValue(null),
                'genero'=> $this->tinyInteger(4)->null()->defaultValue(null),
                'telefone'=> $this->string(50)->null()->defaultValue(null),
                'created_at'=> $this->datetime()->notNull(),
                'created_by'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->timestamp()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
                'updated_by'=> $this->integer(11)->notNull(),
                'deleted_at'=> $this->datetime()->null()->defaultValue(null),
                'deleted_by'=> $this->integer(11)->null()->defaultValue(null),
            ],$tableOptions
        );
        $this->createIndex('email','{{%clientes}}',['email'],true);

    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%clientes}}');
        $this->dropTable('{{%clientes}}');
    }
}
