<?php

use yii\db\Schema;
use yii\db\Migration;

class m181205_072655_categorias extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%categorias}}',
            [
                'id'=> $this->primaryKey(11),
                'nome'=> $this->string(50)->notNull(),
                'status'=> $this->tinyInteger(4)->notNull()->defaultValue(1),
                'created_at'=> $this->datetime()->notNull(),
                'created_by'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->timestamp()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
                'updated_by'=> $this->integer(11)->notNull(),
                'deleted_at'=> $this->datetime()->null()->defaultValue(null),
                'deleted_by'=> $this->integer(11)->null()->defaultValue(null),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%categorias}}');
    }
}
