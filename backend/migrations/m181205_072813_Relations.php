<?php

use yii\db\Schema;
use yii\db\Migration;

class m181205_072813_Relations extends Migration
{

    public function init()
    {
       $this->db = 'db';
       parent::init();
    }

    public function safeUp()
    {
        $this->addForeignKey('fk_projetos_categoria_id',
            '{{%projetos}}','categoria_id',
            '{{%categorias}}','id',
            'RESTRICT','RESTRICT'
         );
        $this->addForeignKey('fk_projetos_cliente_id',
            '{{%projetos}}','cliente_id',
            '{{%clientes}}','id',
            'RESTRICT','RESTRICT'
         );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_projetos_categoria_id', '{{%projetos}}');
        $this->dropForeignKey('fk_projetos_cliente_id', '{{%projetos}}');
    }
}
