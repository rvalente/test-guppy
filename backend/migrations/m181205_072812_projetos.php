<?php

use yii\db\Schema;
use yii\db\Migration;

class m181205_072812_projetos extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%projetos}}',
            [
                'id'=> $this->primaryKey(11),
                'categoria_id'=> $this->integer(11)->notNull(),
                'cliente_id'=> $this->integer(11)->notNull(),
                'nome'=> $this->string(200)->notNull(),
                'descricao'=> $this->text()->null()->defaultValue(null),
                'created_at'=> $this->datetime()->notNull(),
                'created_by'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->timestamp()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
                'updated_by'=> $this->integer(11)->notNull(),
                'deleted_at'=> $this->datetime()->null()->defaultValue(null),
                'deleted_by'=> $this->integer(11)->null()->defaultValue(null),
            ],$tableOptions
        );
        $this->createIndex('cliente_id_projeto','{{%projetos}}',['cliente_id'],false);
        $this->createIndex('categoria_id_projeto','{{%projetos}}',['categoria_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('cliente_id_projeto', '{{%projetos}}');
        $this->dropIndex('categoria_id_projeto', '{{%projetos}}');
        $this->dropTable('{{%projetos}}');
    }
}
