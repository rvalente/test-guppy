<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Categoria;
use common\models\Cliente;
use marqu3s\summernote\Summernote;

/* @var $this yii\web\View */
/* @var $model common\models\Projeto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projeto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'categoria_id')->widget(Select2::classname(), [
        'name' => 'kv-type-01',
        'data' => Categoria::select2(),
        'options' => [
            'placeholder' => 'Selecione uma categoria',
        ],
    ]); ?>

    <?= $form->field($model, 'cliente_id')->widget(Select2::classname(), [
        'name' => 'kv-type-01',
        'data' => Cliente::select2(),
        'options' => [
            'placeholder' => 'Selecione um cliente',
        ],
    ]); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descricao')->widget(Summernote::className(), [
        /*'clientOptions' => [
        ]*/
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
