<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cliente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nome')]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('email')]) ?>

    <?= $form->field($model, 'nascimento')->textInput(['type' => 'date']) ?>

    <div class="form-group">
        <?= Html::activeLabel($model, 'genero'); ?>
        <div class="clip-radio radio-primary">
            <input type="radio" value="0" name="Cliente[genero]" id="gender_female" <?= (!$model->genero) ? 'checked' : '';?> >
            <label for="gender_female">
                Feminino
            </label>
            <input type="radio" value="1" name="Cliente[genero]" id="gender_male"  <?= ($model->genero) ? 'checked' : '';?>>
            <label for="gender_male">
                Masculino
            </label>
        </div>
    </div>

    <?= $form->field($model, 'telefone')->textInput(['maxlength' => true, 'placeholder' => '(xx) xxxxx-xxxx']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
