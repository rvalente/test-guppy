<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Novo Cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nome',
            'email:email',
            [
                'attribute' => 'nascimento',
                'value' => function($model) {
                    return date('d/m/Y', strtotime($model->nascimento));
                }
            ],
            [
                'attribute' => 'genero',
                'format' => 'raw',
                'value' => function($model) {
                    return ($model->genero) ? "<span class='label label-primary'>Masculino</span>" : "<span class='label label-default'>Feminino</span>";
                }
            ],
            'telefone',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            //'deleted_at',
            //'deleted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
