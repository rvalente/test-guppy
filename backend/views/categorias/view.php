<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Categoria */

$this->title = "Detalhes da categoria: {$model->nome}";
$this->params['breadcrumbs'][] = ['label' => 'Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar esse registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nome',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => ($model->status) ? "<span class='label label-success'>Ativo</span>" : "<span class='label label-danger'>Inativo</span>"
            ],
            [
                'attribute' => 'created_at', 
                'value' => date('d/m/Y', strtotime($model->created_at)) . ' às ' . date('H:i', strtotime($model->created_at))
            ],
            [
                'attribute' => 'created_by',
                'value' => User::find()->where(['id' => $model->created_by])->one()->username
            ],
            [
                'attribute' => 'updated_at', 
                'value' => date('d/m/Y', strtotime($model->updated_at)) . ' às ' . date('H:i', strtotime($model->updated_at))],
            [
                'attribute' => 'updated_by',
                'value' => User::find()->where(['id' => $model->updated_by])->one()->username
            ],
        ],
    ]) ?>

</div>
