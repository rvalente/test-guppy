<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categoria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categoria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?php if($model->status !== null): ?>
        <div class="form-group">
            <?= Html::activeLabel($model, 'status'); ?>
            <div class="clip-radio radio-primary">
                <input type="radio" value="1" name="Categoria[status]" id="status-ativo" <?= ($model->status) ? 'checked' : '';?> >
                <label for="gender_female">
                    Ativo
                </label>
                <input type="radio" value="0" name="Categoria[status]" id="status-inativo"  <?= (!$model->status) ? 'checked' : '';?>>
                <label for="gender_male">
                    Inativo
                </label>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
